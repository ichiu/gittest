Git global setup
git config --global user.name "I-Huan Chiu"
git config --global user.email "i-huan.chiu@cern.ch"

Create a new repository
git clone https://gitlab.cern.ch/ichiu/gittest.git
cd gittest
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin https://gitlab.cern.ch/ichiu/gittest.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.cern.ch/ichiu/gittest.git
git push -u origin --all
git push -u origin --tags
